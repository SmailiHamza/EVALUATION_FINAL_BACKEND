package com.zenika.hamza.faq.application;

import com.zenika.hamza.faq.domain.model.user.User;
import com.zenika.hamza.faq.domain.model.user.UserNotFoundException;
import com.zenika.hamza.faq.domain.repository.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@DataJpaTest
public class UserServiceTest {
    @Autowired
    private UserRepository userRepository;
    private UserService userService;
    private User user;



    @BeforeEach
    public void setup(){
        String tid= UUID.randomUUID().toString();
        user = new User(tid,"test@test.jpa","test");
        userService=new UserService(userRepository);
    }

    @Test
    public void should_store_a_user() {
        User savedU = userService.createUser(user.getEmail(), user.getPassword());
        Assertions.assertThat(savedU).isNotNull();
    }
    @Test
    public void should_find_user_by_id() {
        userService.createUser(user.getEmail(),user.getPassword());
        Optional<User> savedU =userService.findByEmail(user.getEmail());
        Optional<User> findedU =userService.findByTid(savedU.get().getTid());
        Assertions.assertThat(findedU).isPresent();

    }
    @Test
    public void should_find_user_by_email() {
        userService.createUser(user.getEmail(),user.getPassword());
        Optional<User> savedU =userService.findByEmail(user.getEmail());
        Assertions.assertThat(savedU).isPresent();

    }
    @Test
    public void should_update_user_by_id() throws UserNotFoundException {
        userService.createUser(user.getEmail(),user.getPassword());
        Optional<User> savedU =userService.findByEmail(user.getEmail());
        userService.changeUsername(savedU.get().getTid(),"testupdate");
        Optional<User> userUpadte=userService.findByTid(savedU.get().getTid());
        assertEquals(userUpadte.get().getPassword(),"testupdate");
    }
    @Test
    public void should_delete_user_by_id() {
        userService.createUser(user.getEmail(),user.getPassword());
        Optional<User> savedU =userService.findByEmail(user.getEmail());
        userService.deleteUser(savedU.get().getTid());
        Optional<User> emptyUser=userService.findByTid(user.getTid());
        assertFalse(emptyUser.isPresent());
    }


}
