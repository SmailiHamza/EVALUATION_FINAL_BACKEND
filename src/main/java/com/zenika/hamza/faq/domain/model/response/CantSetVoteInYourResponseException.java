package com.zenika.hamza.faq.domain.model.response;

public class CantSetVoteInYourResponseException extends Exception{
    public CantSetVoteInYourResponseException() {
        super("Can't set vote in your response");
    }
}
