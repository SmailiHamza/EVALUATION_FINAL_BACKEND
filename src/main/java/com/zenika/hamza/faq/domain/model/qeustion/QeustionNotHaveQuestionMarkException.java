package com.zenika.hamza.faq.domain.model.qeustion;

public class QeustionNotHaveQuestionMarkException extends Exception{
    public QeustionNotHaveQuestionMarkException() {
        super("The question must have question mark !");
    }
}
