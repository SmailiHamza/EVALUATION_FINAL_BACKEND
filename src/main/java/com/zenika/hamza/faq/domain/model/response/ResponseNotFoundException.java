package com.zenika.hamza.faq.domain.model.response;

public class ResponseNotFoundException extends Exception{
    public ResponseNotFoundException() {
        super("Response not found !");
    }
}
