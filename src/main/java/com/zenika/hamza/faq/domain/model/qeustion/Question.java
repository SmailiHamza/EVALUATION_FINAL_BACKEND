package com.zenika.hamza.faq.domain.model.qeustion;

import com.zenika.hamza.faq.domain.model.response.Response;
import com.zenika.hamza.faq.domain.model.user.User;

import javax.persistence.*;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name="qeustion")

public class Question {
    @Id
    private String tid;
    private String title;
    private String content;
    @OrderBy(value = "dates" )
    @Column(name = "dates")
    private String date;
    @Column(name = "languagedev")
    private Language languageDev;
    @OneToOne
    @JoinColumn(name = "user_tid")
    private User user;
    @OneToMany(mappedBy = "question", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    @OrderBy("userVote")
    private List<Response> response;

    public Question() {
    }

    public Question(String tid, String title, String content, String date, Language langauge, User user) {
        this.tid = tid;
        this.title = title;
        this.content = content;
        this.date = date;
        this.languageDev =langauge;
        this.user=user;
    }

    public String getTid() {
        return tid;
    }
    public String getTitle() {
        return title;
    }
    public String getContent() {
        return content;
    }

    public String getDate() {
        return date;
    }
    public Language getLanguageDev() {
        return languageDev;
    }
    public List<Response> getResponseList() {
        if(response!=null){
            try{
                this.response.sort(Comparator.comparing(r -> r.getUserVote().size(),Integer::compare));
            }catch (Exception e){
                //nothing
            }

            Collections.reverse(this.response);
        }

        return this.response;
    }



    public User getUser() {
        return user;
    }
}
