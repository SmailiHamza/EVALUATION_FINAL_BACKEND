package com.zenika.hamza.faq.domain.model.user;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name="users")
public class User implements UserDetails {
    @Id
    private String tid;
    private String email;
    private String pseudo;

    public User(){
        //for JDA
    }

    public User(String tid, String email, String username) {
        this.tid = tid;
        this.pseudo = username;
        this.email = email;
    }

    public String getTid() {
        return tid;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return this.pseudo;
    }

    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setUsername(String username) {
        if(!StringUtils.hasText(username)) {
            throw new IllegalArgumentException("Username can't be null");
        }
        this.pseudo = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if(!StringUtils.hasText(email)) {
            throw new IllegalArgumentException("Email can't be null");
        }
        this.email = email;
    }
}
