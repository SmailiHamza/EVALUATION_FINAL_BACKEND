package com.zenika.hamza.faq.domain.model.qeustion;

import java.util.stream.Stream;

public enum Language {
    Python,JavaScript,Java,Csharp,C,Cplusplus,Go,R,Swift,PHP;
    public static Stream<Language> stream() {
        return Stream.of(Language.values());
    }

}
