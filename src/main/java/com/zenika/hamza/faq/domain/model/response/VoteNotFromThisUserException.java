package com.zenika.hamza.faq.domain.model.response;

public class VoteNotFromThisUserException  extends Exception{
    public VoteNotFromThisUserException() {
        super("Vote not from this user exception");
    }
}
