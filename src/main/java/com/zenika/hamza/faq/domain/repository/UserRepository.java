package com.zenika.hamza.faq.domain.repository;



import com.zenika.hamza.faq.domain.model.user.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User,String> {
   public Optional<User> findByEmail(String email);
}
