package com.zenika.hamza.faq.domain.model.response;


import com.zenika.hamza.faq.domain.model.qeustion.Question;
import com.zenika.hamza.faq.domain.model.user.User;

import javax.persistence.*;
import java.util.List;

@Entity
public class Response {
    @Id
    @Column(name="tid")
    private String tid;
    @Column(name = "content")
    private String content;
    @Column(name = "dates")
    private String date;
    @OneToOne
    @JoinColumn(name = "user_tid")
    private User user;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "qeustion_tid")
    private Question question;
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "vote", joinColumns = @JoinColumn(name = "response_tid"))
    @Column(name = "user_tid")
    private List<String> userVote;
    public Response() {
    }
    public Response(String tid, String content, String date,User user,Question question) {
        this.tid=tid;
        this.content = content;
        this.date = date;
        this.user=user;
        this.question=question;
    }

    public String getTid() {
        return tid;
    }
    public List<String> getUserVote() {
        return userVote;
    }
    public Integer getNbVote() {
        if(userVote!=null)
        return userVote.size();
        return 0;
    }
    public String getContent() {
        return content;
    }
    public String getDate() {
        return date;
    }
    public String getEmail() {
        return user.getEmail();
    }
    public String getPseudo() {
        return user.getPassword();
    }
    public String getUserTid() {
        return user.getTid();
    }

}
