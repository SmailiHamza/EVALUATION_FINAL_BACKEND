package com.zenika.hamza.faq.domain.repository;

import com.zenika.hamza.faq.domain.model.response.Response;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ResponseRepository extends CrudRepository<Response,String> {
    @Query(value = "SELECT * FROM response WHERE user_tid=?1 ", nativeQuery = true)
    List<Response> findByUserTid(String userTid);
}
