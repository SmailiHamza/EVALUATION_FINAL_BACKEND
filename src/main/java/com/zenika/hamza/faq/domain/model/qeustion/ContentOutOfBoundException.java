package com.zenika.hamza.faq.domain.model.qeustion;

public class ContentOutOfBoundException extends Exception{
    public ContentOutOfBoundException() {
        super("Content must have 100 words max");
    }
}
