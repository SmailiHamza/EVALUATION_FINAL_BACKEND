package com.zenika.hamza.faq.domain.model.qeustion;

import org.hibernate.exception.ConstraintViolationException;

import java.sql.SQLException;

public class CustomConstraintViolationException extends ConstraintViolationException {
    String message;
    SQLException root;
    String constraintName;

    public CustomConstraintViolationException(String message, SQLException root, String constraintName) {
        super(message, root, constraintName);
        this.message = message;
        this.root = root;
        this.constraintName = constraintName;
    }

    public String getMessage() {
        String message = this.message;
        if (this.root.getMessage().contains("duplicate key value violates") && this.root.getMessage().contains("response_pkey")) {
            message = "Can't post the same response";
        }
        return message;

    }
}
