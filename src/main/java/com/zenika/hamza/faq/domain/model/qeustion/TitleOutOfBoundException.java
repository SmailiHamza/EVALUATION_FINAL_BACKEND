package com.zenika.hamza.faq.domain.model.qeustion;

public class TitleOutOfBoundException extends Exception{
    public TitleOutOfBoundException() {
        super("Title must have 20 words max");
    }
}
