package com.zenika.hamza.faq.domain.repository;

import com.zenika.hamza.faq.domain.model.qeustion.Language;
import com.zenika.hamza.faq.domain.model.qeustion.Question;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
@Repository
public interface QeustionRepository extends PagingAndSortingRepository<Question, String> {

    @Query(value = "SELECT * FROM qeustion WHERE SIMILARITY(title,?1) > 0.3", nativeQuery = true)
    List<Question> searchByTitleLike(String title);
    List<Question> findByLanguageDev(Language langauge);
     List<Question> findByUserTid(String userTid);
    @Query(value = "SELECT * FROM qeustion q FULL JOIN response r ON q.tid=r.qeustion_tid WHERE r.qeustion_tid IS NULL", nativeQuery = true)
    List<Question> findWhereResponseListIsNull();


}
