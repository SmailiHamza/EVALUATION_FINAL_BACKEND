package com.zenika.hamza.faq.web.user;


import com.zenika.hamza.faq.application.UserService;
import com.zenika.hamza.faq.domain.model.user.EmailAlreadyUsedException;
import com.zenika.hamza.faq.domain.model.user.User;
import com.zenika.hamza.faq.domain.model.user.UserNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/users")
@CrossOrigin("http://localhost:4200/")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    ResponseEntity<User> createUser(@RequestBody CreateUserRequestDto body) {
        User createdUser = userService.createUser(body.email(), body.pseudo());
        return ResponseEntity.status(HttpStatus.CREATED).body(createdUser);
    }

    @PostMapping("/auth")
    ResponseEntity<CreateUserRequestDto> auth() throws UserNotFoundException {
        User user = userService.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(UserNotFoundException::new);
        CreateUserRequestDto createUserRequestDto=new CreateUserRequestDto(user.getUsername(), user.getPassword(),user.getTid());
        return ResponseEntity.status(HttpStatus.OK).body(createUserRequestDto);
    }
    @PostMapping("/signup")
    ResponseEntity<CreateUserRequestDto> signUp(@RequestBody CreateUserRequestDto body) {
        System.out.println(body.toString());
        User user=userService.createUser(body.email(), body.pseudo());
        CreateUserRequestDto createUserRequestDto=new CreateUserRequestDto(user.getUsername(), user.getPassword(),user.getTid());
        return ResponseEntity.status(HttpStatus.OK).body(createUserRequestDto);
    }

    @GetMapping("/{userTid}")
    ResponseEntity<User> findUser(@PathVariable String userTid) {
        return userService.findByTid(userTid).map(u -> ResponseEntity.ok(u)).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping
    List<User> searchUsers(@RequestParam("email") String email) {
        return userService.findByEmail(email)
                .map(List::of)
                .orElseGet(Collections::emptyList);
    }

    @PatchMapping("/{userTid}")
    ResponseEntity<Void> changeUsername(@PathVariable String userTid, @RequestBody ChangeUsernameRequestDto body) {
        try {
            userService.changeUsername(userTid, body.newPseudo());
            return ResponseEntity.ok().build();
        } catch (UserNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{userTid}")
    @ResponseStatus(HttpStatus.OK)
    void deleteUser(@PathVariable String userTid) {
        userService.deleteUser(userTid);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleIllegalArgument(IllegalArgumentException e) {
        return "Missing information : " + e.getMessage();
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.CONFLICT)
    public String handleIllegalArgument(EmailAlreadyUsedException e) {
        return "Email already exists";
    }
    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleUserNotFound(UserNotFoundException e) {
        return "User not found";
    }
}
