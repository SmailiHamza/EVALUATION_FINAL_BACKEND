package com.zenika.hamza.faq.web.user;

public record CreateUserRequestDto(String email, String pseudo,String tid) {
}
