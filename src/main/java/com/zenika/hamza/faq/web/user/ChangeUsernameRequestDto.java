package com.zenika.hamza.faq.web.user;

public record ChangeUsernameRequestDto(String newPseudo) {
}
