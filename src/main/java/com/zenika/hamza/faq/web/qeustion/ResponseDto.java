package com.zenika.hamza.faq.web.qeustion;

import java.util.List;

public record ResponseDto(String tid, String content, String date, String userTid, List<String> userVote,Integer nbVote) {
}
