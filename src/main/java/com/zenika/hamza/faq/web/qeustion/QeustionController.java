package com.zenika.hamza.faq.web.qeustion;

import com.zenika.hamza.faq.application.QeustionManager;
import com.zenika.hamza.faq.application.ResponseManager;
import com.zenika.hamza.faq.domain.model.qeustion.*;
import com.zenika.hamza.faq.domain.model.response.CantSetVoteInYourResponseException;
import com.zenika.hamza.faq.domain.model.response.Response;
import com.zenika.hamza.faq.domain.model.response.ResponseNotFoundException;
import com.zenika.hamza.faq.domain.model.response.VoteNotFromThisUserException;
import com.zenika.hamza.faq.domain.model.user.UserNotFoundException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/qeustions")
@CrossOrigin(value = "http://localhost:4200/")
public class QeustionController {
private final QeustionManager qeustionManager;
    private final ResponseManager responseManager;

    public QeustionController(QeustionManager qeustionManager, ResponseManager responseManager) {
        this.qeustionManager = qeustionManager;
        this.responseManager = responseManager;
    }
    @PostMapping
    ResponseEntity<QeustionDto> createQeustion(@RequestBody QeustionDto body) throws TitleOutOfBoundException, QeustionNotHaveQuestionMarkException, UserNotFoundException {
        Question createdQeustion = this.qeustionManager.save(body.title(), body.content(), SecurityContextHolder.getContext().getAuthentication().getName(),body.language());
        return ResponseEntity.status(HttpStatus.CREATED).body(new QeustionDto(createdQeustion.getTid(),createdQeustion.getTitle(),createdQeustion.getContent(),createdQeustion.getDate(),createdQeustion.getLanguageDev(),createdQeustion.getResponseList(),createdQeustion.getUser().getTid(),createdQeustion.getUser().getEmail(),createdQeustion.getUser().getPassword()));
    }
    @PutMapping("/{qeustionTid}/response")
    ResponseEntity<QeustionDto> addResponseToQeustion(@PathVariable String qeustionTid,@RequestBody ResponseDto body) throws ContentOutOfBoundException, QeustionNotFoundException, UserNotFoundException {
        Question createdQeustion = this.qeustionManager.addResponse(qeustionTid,body);
        return ResponseEntity.status(HttpStatus.CREATED).body(new QeustionDto(createdQeustion.getTid(),createdQeustion.getTitle(),createdQeustion.getContent(),createdQeustion.getDate(),createdQeustion.getLanguageDev(),createdQeustion.getResponseList(),createdQeustion.getUser().getTid(),createdQeustion.getUser().getEmail(),createdQeustion.getUser().getPassword()));
    }
    @GetMapping
    ResponseEntity<PageableDto> findAll(@RequestParam(defaultValue = "0") Integer pageNo)  {
        return ResponseEntity.ok(new PageableDto(StreamSupport.stream(qeustionManager.findAll(pageNo).spliterator(), false).map(q -> new QeustionDto(q.getTid(),q.getTitle(),q.getContent(),q.getDate(),q.getLanguageDev(), q.getResponseList(), q.getUser().getTid(),q.getUser().getEmail(),q.getUser().getPassword())).collect(Collectors.toList()),qeustionManager.findAll(pageNo).getTotalPages()));
    }
    @GetMapping("/{qeustionTid}")
    ResponseEntity <QeustionDto> findByTid(@PathVariable String qeustionTid) throws QeustionNotFoundException {
        Question q=qeustionManager.findByTid(qeustionTid);
        QeustionDto qeustionDto=new QeustionDto(q.getTid(),q.getTitle(),q.getContent(),q.getDate(),q.getLanguageDev(),q.getResponseList(),q.getUser().getTid(),q.getUser().getEmail(),q.getUser().getPassword());
        return ResponseEntity.ok(qeustionDto);
    }
    @GetMapping("/title")
    ResponseEntity<List<QeustionDto>> searchByTilte(@RequestParam String title) {
        return ResponseEntity.ok(qeustionManager.searchByTitle(title).stream().map(q -> new QeustionDto(q.getTid(),q.getTitle(),q.getContent(),q.getDate(),q.getLanguageDev(),q.getResponseList(),q.getUser().getTid(),q.getUser().getEmail(),q.getUser().getPassword())).collect(Collectors.toList()));
    }
    @GetMapping("/language")
    Stream<Language> getLanguage() {
        return Language.stream();
    }
    @GetMapping("/language/search")
    ResponseEntity<List<QeustionDto>> getLanguage(@RequestParam Language language) {
        return ResponseEntity.ok(qeustionManager.findByLanguageDev(language).stream().map(q -> new QeustionDto(q.getTid(),q.getTitle(),q.getContent(),q.getDate(),q.getLanguageDev(),q.getResponseList(),q.getUser().getTid(),q.getUser().getEmail(),q.getUser().getPassword())).collect(Collectors.toList()));
    }
    @GetMapping("/user/me")
    ResponseEntity<List<QeustionDto>> findByUserTid(@RequestParam String userTid) {
        return ResponseEntity.ok(qeustionManager.findByUserTid(userTid).stream().map(q -> new QeustionDto(q.getTid(),q.getTitle(),q.getContent(),q.getDate(),q.getLanguageDev(),q.getResponseList(),q.getUser().getTid(),q.getUser().getEmail(),q.getUser().getPassword())).collect(Collectors.toList()));
    }
    @GetMapping("/questionwithoutresponse")
    ResponseEntity<List<QeustionDto>> findByWhereResponseListIsNull() {
        return ResponseEntity.ok(qeustionManager.findWhereResponseListIsNull().stream().map(q -> new QeustionDto(q.getTid(),q.getTitle(),q.getContent(),q.getDate(),q.getLanguageDev(),q.getResponseList(),q.getUser().getTid(),q.getUser().getEmail(),q.getUser().getPassword())).collect(Collectors.toList()));
    }
    @PostMapping("/response/vote")
    ResponseEntity<Response> saveVote(@RequestBody VoteDto  body) throws UserNotFoundException, CantSetVoteInYourResponseException, ResponseNotFoundException {
        return ResponseEntity.ok(responseManager.saveVote(body.responseTid()));
    }
    @GetMapping("/response/user/{userTid}")
    ResponseEntity<List<Response>> findResponseByUserTid(@PathVariable String userTid) throws UserNotFoundException, CantSetVoteInYourResponseException, ResponseNotFoundException {
        return ResponseEntity.ok(responseManager.findByUserTid(userTid));
    }
    @DeleteMapping("/response/vote/{responseTid}")
    ResponseEntity<Response> deleteVote(@PathVariable String responseTid) throws UserNotFoundException, VoteNotFromThisUserException, ResponseNotFoundException {
        return ResponseEntity.ok(responseManager.deleteVote(responseTid));
    }
    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleTitleOutOfBoundException(TitleOutOfBoundException e) {
        return e.getMessage();
    }
    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleContentOutOfBoundException(ContentOutOfBoundException e) {
        return e.getMessage();
    }
    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleQeustionNotHaveQuestionMarkException(QeustionNotHaveQuestionMarkException e) {
        return e.getMessage();
    }
    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleQeustionNotFoundException(QeustionNotFoundException e) {
        return e.getMessage();
    }
    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleUserNotFound(UserNotFoundException e) {
        return "User not found";
    }
    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleConstraintViolationException(ConstraintViolationException e) {
        CustomConstraintViolationException constraintViolationException = new CustomConstraintViolationException(e.getMessage(), e.getSQLException(), e.getConstraintName());
        return constraintViolationException.getMessage();
    }
    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleVoteNotFromThisUserException(VoteNotFromThisUserException e) {
           return e.getMessage();
    }
    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleCantSetVoteInYourResponseException(CantSetVoteInYourResponseException e) {
        return e.getMessage();
    }
    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleResponseNotFoundException(ResponseNotFoundException e) {
        return e.getMessage();
    }
}
