package com.zenika.hamza.faq.web.qeustion;

import com.zenika.hamza.faq.domain.model.qeustion.Language;
import com.zenika.hamza.faq.domain.model.response.Response;

import java.util.List;

public record QeustionDto (String tid, String title, String content, String date, Language language, List<Response> responseList, String userTid, String userPseudo, String userEmail){
}
