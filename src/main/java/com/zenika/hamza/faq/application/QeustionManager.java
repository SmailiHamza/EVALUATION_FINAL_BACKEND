package com.zenika.hamza.faq.application;

import com.zenika.hamza.faq.domain.model.qeustion.*;
import com.zenika.hamza.faq.domain.model.response.Response;
import com.zenika.hamza.faq.domain.model.user.User;
import com.zenika.hamza.faq.domain.model.user.UserNotFoundException;
import com.zenika.hamza.faq.domain.repository.QeustionRepository;
import com.zenika.hamza.faq.domain.repository.UserRepository;
import com.zenika.hamza.faq.web.qeustion.ResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Service
public class QeustionManager {
    private final QeustionRepository qeustionRepository;
    private final UserRepository userRepository;
    public QeustionManager(QeustionRepository qeustionRepository, UserRepository userRepository) {
        this.qeustionRepository = qeustionRepository;
        this.userRepository = userRepository;
    }
    public Question save(String title, String content, String email, Language language) throws TitleOutOfBoundException, QeustionNotHaveQuestionMarkException, UserNotFoundException {
        if(title.split(" ").length>20){
            throw new TitleOutOfBoundException();
        }
        if(!title.matches(".*\\?$")){
            throw new QeustionNotHaveQuestionMarkException();
        }
        String date= String.valueOf(Instant.now().toEpochMilli());
        User user=this.userRepository.findByEmail(email).orElseThrow(UserNotFoundException::new);

        Question question=new Question(UUID.randomUUID().toString(),title,content,date,language,user);
        return qeustionRepository.save(question);
    }
    public Question addResponse(String tid, ResponseDto responseDto) throws ContentOutOfBoundException, QeustionNotFoundException, UserNotFoundException {
        String date= String.valueOf(Instant.now().toEpochMilli());
        if(responseDto.content().split(" ").length>100){
            throw new ContentOutOfBoundException();
        }

        User user=userRepository.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(UserNotFoundException::new);
        Question question=findByTid(tid);
        Response response=new Response(UUID.randomUUID().toString(),responseDto.content(),date,user,question);


        question.getResponseList().add(response);
        return qeustionRepository.save(question);
    }
    public Question findByTid(String tid) throws QeustionNotFoundException {
        return this.qeustionRepository.findById(tid).orElseThrow(QeustionNotFoundException::new);
    }

    public Page<Question> findAll(Integer pageNo) {
        return this.qeustionRepository.findAll(PageRequest.of(pageNo, 2, Sort.by("date").descending()));
    }

    public List<Question> searchByTitle(String title) {
        return qeustionRepository.searchByTitleLike(title);
    }
    public List<Question> findByLanguageDev(Language langauge) {
        return qeustionRepository.findByLanguageDev(langauge);
    }
    public List<Question> findByUserTid(String userTid) {
        return qeustionRepository.findByUserTid(userTid);
    }
    public List<Question> findWhereResponseListIsNull() {
        return qeustionRepository.findWhereResponseListIsNull();
    }
}
