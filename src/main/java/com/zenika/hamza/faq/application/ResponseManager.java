package com.zenika.hamza.faq.application;

import com.zenika.hamza.faq.domain.model.response.CantSetVoteInYourResponseException;
import com.zenika.hamza.faq.domain.model.response.Response;
import com.zenika.hamza.faq.domain.model.response.ResponseNotFoundException;
import com.zenika.hamza.faq.domain.model.response.VoteNotFromThisUserException;
import com.zenika.hamza.faq.domain.model.user.User;
import com.zenika.hamza.faq.domain.model.user.UserNotFoundException;
import com.zenika.hamza.faq.domain.repository.ResponseRepository;
import com.zenika.hamza.faq.domain.repository.UserRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResponseManager {
    private final ResponseRepository responseRepository;
    private final UserRepository userRepository;
    public ResponseManager(ResponseRepository responseRepository, UserRepository userRepository) {
        this.responseRepository = responseRepository;
        this.userRepository = userRepository;
    }
    public List<Response> findByUserTid(String userTid) throws UserNotFoundException {
        User user=userRepository.findById(userTid).orElseThrow(UserNotFoundException::new);
        return responseRepository.findByUserTid(userTid);
    }
    public Response saveVote(String responseTid) throws UserNotFoundException, CantSetVoteInYourResponseException, ResponseNotFoundException {
        User user=userRepository.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(UserNotFoundException::new);
        Response response=responseRepository.findById(responseTid).orElseThrow(ResponseNotFoundException::new);
        if(user.getTid().equals(response.getUserTid())){
            throw new CantSetVoteInYourResponseException();
        }else{
            response.getUserVote().add(user.getTid());
           return responseRepository.save(response);
        }
    }
    public Response deleteVote(String responseTid) throws UserNotFoundException, VoteNotFromThisUserException, ResponseNotFoundException {
        User user=userRepository.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(UserNotFoundException::new);
        Response response=responseRepository.findById(responseTid).orElseThrow(ResponseNotFoundException::new);

        if(!response.getUserVote().contains(user.getTid())){
            throw new VoteNotFromThisUserException();
        }else{
            response.getUserVote().remove(user.getTid());
            return responseRepository.save(response);
        }
    }
}
