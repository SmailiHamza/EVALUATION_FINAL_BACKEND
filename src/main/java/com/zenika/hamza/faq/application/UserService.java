package com.zenika.hamza.faq.application;



import com.zenika.hamza.faq.domain.model.user.EmailAlreadyUsedException;
import com.zenika.hamza.faq.domain.model.user.User;
import com.zenika.hamza.faq.domain.model.user.UserNotFoundException;
import com.zenika.hamza.faq.domain.repository.UserRepository;
import org.springframework.stereotype.Service;


import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User createUser(String email, String username) {
        User newUser = new User(UUID.randomUUID().toString(), email, username);
        try {
            userRepository.save(newUser);
        } catch (Exception e) {
            throw new EmailAlreadyUsedException();
        }


        return newUser;
    }

    public Optional<User> findByTid(String userTid) {
        return userRepository.findById(userTid);
    }

    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public void changeUsername(String userTid, String newUsername) throws UserNotFoundException {
        User user = userRepository.findById(userTid).orElseThrow(UserNotFoundException::new);
        user.setUsername(newUsername);
        userRepository.save(user);
    }

    public void deleteUser(String userTid) {
        userRepository.deleteById(userTid);
    }

}
