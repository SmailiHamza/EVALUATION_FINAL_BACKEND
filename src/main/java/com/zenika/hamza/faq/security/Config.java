package com.zenika.hamza.faq.security;


import com.zenika.hamza.faq.domain.model.user.User;
import com.zenika.hamza.faq.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Configuration
public class Config {
    @Autowired
    UserRepository userRepository;
    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

        return http
                .antMatcher("/**")
                .csrf().disable()// CSRF protection is enabled by default in the Java configuration.
                .authorizeRequests(authorize -> authorize
                        .antMatchers(HttpMethod.GET, "/qeustions").permitAll()
                        .antMatchers(HttpMethod.GET, "/qeustions/*").permitAll()
                        .antMatchers(HttpMethod.GET, "/qeustions/user/me").permitAll()
                        .antMatchers(HttpMethod.GET, "/qeustions/response/user/*").permitAll()
                        .antMatchers(HttpMethod.POST, "/users/signup").permitAll()
                        .antMatchers(HttpMethod.GET, "/qeustions/language/search").permitAll()
                        .anyRequest().authenticated()
                )
                .httpBasic()
                .and()
                .cors()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
               // .addFilterAfter(httpFilter(), BasicAuthenticationFilter.class)
                .build();
    }

//    @Bean
//    public UserDetailsService users() {
//        UserDetails admin = User.builder()
//                .username("admin")
//                .password("{noop}admin")
//                .roles("USER", "ADMIN")
//                .build();
//        UserDetails user = User.builder()
//                .username("root")
//                .password("{noop}root")
//                .roles("USER")
//                .build();
//        return new InMemoryUserDetailsManager(admin, user);
//    }

    @Bean
    UserDetailsService userDetailsService() {
        return new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
                Optional<User> user = userRepository.findByEmail(email);
                return userRepository.findByEmail(email).orElseThrow(()-> new UsernameNotFoundException("User not fpund"));
            }
        };
    }
    @Bean
    PasswordEncoder passwordEncoder(){
        return new PasswordEncoder() {
            @Override
            public String encode(CharSequence rawPassword) {
                return rawPassword.toString();
            }

            @Override
            public boolean matches(CharSequence rawPassword, String encodedPassword) {
                return rawPassword.toString().equals(encodedPassword);
            }
        };
    }


}
