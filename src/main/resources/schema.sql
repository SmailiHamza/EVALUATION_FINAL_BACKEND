
create table if not exists users(
    tid char(36) primary key,
    email text not null unique,
    pseudo text not null unique
);
create table if not exists qeustion(
    tid char(36) primary key,
    title text not null ,
    content text not null,
    dates text not null,
    languagedev integer not null,
    user_tid char(36)  references users(tid) not null
);

create table if not exists response(
    tid char(36)  primary key,
    content text not null,
    dates text not null,
    user_tid char(36)  references users(tid) not null,
    qeustion_tid char(36)  references qeustion(tid) not null
);
create table if not exists vote(
    user_tid char(36)  references users(tid) not null,
    response_tid char(36)  references response(tid) not null,
    primary key(user_tid,response_tid)
);

CREATE EXTENSION if not exists pg_trgm ;

