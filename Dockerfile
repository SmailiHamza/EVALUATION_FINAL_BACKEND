FROM maven:3.8.1-openjdk-17 AS builder

WORKDIR /app

COPY pom.xml .

RUN mvn -e -B dependency:resolve

COPY src ./src

RUN mvn clean -e -B package


FROM openjdk:slim

WORKDIR /app

COPY --from=builder /app/target/*.jar ./app.jar

ENTRYPOINT ["java", "-jar", "./app.jar"]
